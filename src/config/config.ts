import * as dotenv from 'dotenv';

dotenv.config();

export default {
    MONGO: {
        URI: process.env.REMOTEMONGO,
        DBNAME: process.env.DB,
        USER: process.env.USER,
        PASS: process.env.PASS
    },
    APP: {
        PORT: process.env.PORT || 7000
    }
}