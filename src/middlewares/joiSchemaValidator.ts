import { Schema } from '@hapi/joi';
import { Request, Response, NextFunction } from 'express';

export default function joiSchemaValidator(schema: Schema) {

    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {

        try {
            await schema.validateAsync(req.body, { 'context': { 'method': req.method }, abortEarly: true, });
            return next();
        } catch (err) {
            console.log(err.message);
            res.status(422).send({ 'message': 'validation error with joi', 'detail':
                err.message
             });
        }
    }
}