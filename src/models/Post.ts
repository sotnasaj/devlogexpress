import { Schema, Document, model, Types } from 'mongoose';

import {commentSchema, IComment} from './Comment';

export interface IPost extends Document {
    tittle: string,
    content: string,
    user: number,
    synopsis: string,
    creationdate: Date,
    lastupdate: Date,
    tags: Types.Array<string>,
    bravos: number,
    active: boolean,
    comments:Types.Array<IComment>
}

export const postSchema = new Schema<IPost>({
    tittle: { type: String, required: true },
    content: { type: String, required: true },
    user: { type: Number, required: true },
    synopsis: { type: String, required: true },
    creationdate: { type: Date, default: Date.now, },
    lastupdate: { type: Date, default: Date.now, },
    tags: { type: [String], required: false, validate: [tagLimit,'10 tags max'] },
    bravos: { type: Number, required: false },
    active: { type: Boolean, required: false },
    comments: { type: [commentSchema], required: false },
});

function tagLimit(val: [string]) {
    return val.length <= 10;
}

export default model<IPost>('Post', postSchema);