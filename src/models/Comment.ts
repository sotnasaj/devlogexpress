import { Schema, Document, model } from 'mongoose';

export interface IComment extends Document {
    content: string,
    user: number,
    creationdate: Date,
    bravos: number
}

export const commentSchema = new Schema({
    content: String,
    user: { type: Number, required: true },
    creationdate: { type: Date, default: Date.now },
    bravos: { type: Number, required: false }
})

export default model<IComment>('Comment', commentSchema);