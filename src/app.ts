import * as express from 'express';
import * as morgan from 'morgan';

import config from './config/config';
import { postRouter } from './routes/'

const app = express();
// Settings
app.set('port', config.APP.PORT || 7000);

//Middleware
app.use(express.json());
//JUST DEVELOP MODE
if (app.get('env') === 'development') {
    app.use(morgan('dev'));
}

//routes
app.use('/posts', postRouter);

// 404
app.use((_, res) => res.status(404).send({ 'message': 'resource not found' }));

export default app;