import { Router } from 'express';

import * as postController from '../controllers/postController';
import * as commentController from '../controllers/commentController'
import { PostSchema, CommentSchema, validateTag } from '../schemas';
import joiSchemaValidator from '../middlewares/joiSchemaValidator';

export const router = Router();

// POST
router.get('/', postController.getList);
router.get('/:id', postController.findById);
router.post('/', joiSchemaValidator(PostSchema), postController.create);
router.patch('/:id([0-9a-fA-F]{24})', joiSchemaValidator(PostSchema), postController.update);
router.delete('/:id([0-9a-fA-F]{24})', postController.remove);

//POST-TAGS
router.get('/:id([0-9a-fA-f]{24})/tags/', postController.getTags);

router.post('/:id([0-9a-fA-f]{24})/tags/',
    joiSchemaValidator(validateTag),
    postController.addTag
);
router.delete('/:id([0-9a-fA-f]{24})/tags/:tag([0-9a-zA-Z]{1,15})',
    postController.removeTag
);

//POST-COMMENTS
router.get(
    '/:id([0-9a-fA-F]{24})/comments',
    commentController.getList
);
router.get(
    '/:id([0-9a-fA-F]{24})/comments/:comment_id([0-9a-fA-F]{24})',
    commentController.findById
);
router.post(
    '/:id([0-9a-fA-f]{24})/comments/',
    joiSchemaValidator(CommentSchema),
    commentController.create
);
router.delete(
    '/:id([0-9a-fA-F]{24})/comments/:comment_id([0-9a-fA-F]{24})',
    commentController.remove
);