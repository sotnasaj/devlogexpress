import * as Joi from '@hapi/joi';

export const PostSchema = Joi.when(Joi.ref('$method'), {
    'is': 'POST',
    'then': Joi.object({

        tittle: Joi.string()
            .min(10)
            .max(120)
            .required(),

        synopsis: Joi.string()
            .min(25)
            .max(280),

        user: Joi.number()
            .required(),

        content: Joi.string()
            .min(25)
            .required(),

        tags: Joi.array()
            .min(1)
            .max(10)
            .unique()
            .required(),
    }),
    'otherwise': Joi.object({

        tittle: Joi.string()
            .min(10)
            .max(120),

        synopsis: Joi.string()
            .min(25)
            .max(280),

        content: Joi.string()
            .min(1),

        tags: Joi.array()
            .min(1)
            .max(10)
            .unique()

    })
});

export const validateTag =  Joi.object({
    tag: Joi.string()
    .required()
    .min(1)
    .max(10),
});
