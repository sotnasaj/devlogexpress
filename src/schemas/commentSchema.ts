import * as Joi from '@hapi/joi';

export const CommentSchema = Joi.when(Joi.ref('$method'), {
    'is': 'POST',
    'then': Joi.object({

        content: Joi.string()
            .min(1)
            .required(),

        user: Joi.number()
            .required(),
    }),
    'otherwise': Joi.object({

        content: Joi.string()
            .min(1)
            .required(),
    })
})
