import app from './app';
import * as mongoose from 'mongoose';

import config from './config/config';

mongoose.connect(config.MONGO.URI as string, {
    dbName: config.MONGO.DBNAME,
    user: config.MONGO.USER,
    pass: config.MONGO.PASS,
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection
    .on('error', (err) => {
        console.error(err);
        process.exit(0);
    });

mongoose.connection.once('open', () => {
    console.log('Conected to MongoDB');
    app.listen(app.get('port'), () => console.log(`App listening on ${app.get('port')}`));
});