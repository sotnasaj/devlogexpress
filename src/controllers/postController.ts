import { Request, Response } from 'express';

import Post from '../models/Post';

export async function findById(req: Request, res: Response): Promise<void> {
    const result = await Post.findById(req.params.id);
    if (!result) {
        res.status(404).send({ 'message': 'resource not found' });
        return;
    }
    res.send(result);
}
// Pagination not yet '_' to avoid TS error
export async function getList(_: Request, res: Response): Promise<void> {

    const result = await Post.find().limit(50).sort({ 'creationdate': -1 });
    res.send(result);
}

export async function create(req: Request, res: Response): Promise<void> {
    try {
        const post = new Post(req.body);
        const result = await post.save();
        res.status(201).send(result);
    } catch (err) {
        if (err.name === 'ValidationError') {
            res.status(422).send({ 'message': 'validation error' });
        } else {
            console.log(err);
            res.status(500).send({ 'message': 'Server error' });
        }
    }
}

export async function update(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        post.set(req.body);
        post.lastupdate.setDate(Date.now());
        const result = await post.save();
        res.send(result);
    } catch (err) {
        if (err.name === 'ValidationError') {
            res.status(422).send({ 'message': 'validation error' });
        } else {
            console.log(err);
            res.status(500).send({ 'message': 'Server error' });
        }
    }
}

export async function remove(req: Request, res: Response): Promise<void> {
    await Post.deleteOne({ _id: req.params.id });
    res.status(204).send();
}

export async function getTags(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        res.send(post.tags);
    } catch (err) {
        res.status(500).send({ 'message': 'Server error' });
    }
}
// Its only managed by the author, so no race
export async function addTag(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        if (post.tags.includes(req.body.tag)) {
            res.send(post.tags);
            return;
        }
        post.tags.push(req.body.tag);
        await post.save();
        res.send(post.tags);
    } catch (err) {
        if (err.name == 'ValidationError') {
            res.status(422).send({ 'message': 'validation error' });
        } else {
            res.status(500).send({ 'message': 'Server error' });
        }
    }
}

// Its only managed by the author, so no race
export async function removeTag(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(204).send();
            return;
        }
        post.tags.remove(req.params.tag);
        post.save();
        res.send(post.tags);
    } catch (error) {
        res.status(500).send({ 'message': 'Server error' });
    }
}