import { Request, Response } from 'express';

import Post from '../models/Post';

export async function getList(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        res.send(post.comments);
    } catch (err) {
        res.status(500).send({ 'message': 'Server error' });
    }
}
// Race condition? use $push --- create or add? 
export async function create(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        post.comments.push(req.body);
        await post.save();
        res.send();
    } catch (err) {
        if (err.name == 'ValidationError') {
            res.status(422).send({ 'message': 'validation error' });
        } else {
            res.status(500).send({ 'message': 'Server error' });
        }
    }
}

// Race condition? use $pull
export async function remove(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(204).send();
            return;
        }
        post.comments.remove({ '_id': req.params.comment_id });
        post.save();
        res.status(204).send();
    } catch (error) {
        res.status(500).send({ 'message': 'Server error' });
    }
}

// Race condition? use $pull
export async function findById(req: Request, res: Response): Promise<void> {
    try {
        const post = await Post.findById(req.params.id);
        if (!post) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        console.log('encontre el post');
        const comment = post.comments.find((comment) => {

            return comment._id == req.params.comment_id
        });
        if (!comment) {
            res.status(404).send({ 'message': 'resource not found' });
            return;
        }
        res.send(comment);
    } catch (error) {
        res.status(500).send({ 'message': 'Server error' });
    }
}