Devlog JSON API server, maded with love and:

- node.js
- express
- mongodb
- mongoose
- Joi
- Morgan

The JSON API server runs on port 7000, you can change that on .env on src directory


## RUNNING THE APP PD: >>> /SRC <<< DIRECTORY

run app with ts-node (npm installation required)
#### - ***ts-node index.ts***  or ***npx ts-node index.ts*** if you want to use TS files

or you can run the compiled JS files after ***tsc*** with:
#### - ***node index.js*** 

#### you ***need*** to ***have*** the ***.env file on the same directory of index.js/ts***

### Other details
Node app running on 7000 port by defualt (.env file).

## EXAMPLES:

### POST ---> Delete at end

#### Get list of posts
- http://localhost:7000/posts   [GET]  

#### Find post by id, example included
- http://localhost:7000/posts/5ecff97696326339cee9916a  [GET] 

#### Update a post , previous if you want
- http://localhost:7000/posts/5ecff97696326339cee9916a ---> [PATCH]+[JSON]  

```json
{
	"synopsis": "Pew News, the most reliable news source on the internet. Hosted by Poppy Gloria"
}
```
#### Create a new post, so the new _id can be used later
- http://localhost:7000/posts  [POST] + [JSON BODY]

```json
{
	"tittle": "Para agregar/quitar comentario y luego borrar",
	"content": " El cangrejo de los cocoteros (Birgus latro) ... lo que le permite alcanzar hasta un metro de la cabeza a las patas ",
	"user": 31852963,
	"synopsis": "This is a synopsis of a new post with some random data about a cangrejo",
	"tags": [
		"numbers",
		"amelia",
		"series"
	]
} 
```
### Lets try some tags

#### Get tags

- http://localhost:7000/posts/5ecff97696326339cee9916a/tags [GET]

#### Add a tag

- http://localhost:7000/posts/5ecff97696326339cee9916a/tags [POST]
```
{
	"tag": "OP"
}
```

#### Remove a tag

- http://localhost:7000/posts/5ecff97696326339cee9916a/tags/Tutorial [DELETE]

### Now comments

#### Get comments ((id post included))
- http://localhost:7000/posts/5ecff97696326339cee9916a/comments [GET]

#### Find a comment by id (id post/comment included)

- http://localhost:7000/posts/5ecff97696326339cee9916a/comments/5ed0885e382c8b9c364fdd5b [GET]

#### Add a comment to post (id post included)

- http://localhost:7000/posts/5ecff97696326339cee9916a/comments  [POST] + [JSON] 

```json 
{
	"content": "F ",
	"user": 61221
}
```

#### Remove a comment (comment included)

- http://localhost:7000/posts/5ecff97696326339cee9916a/comments/5ed130c1d43104b8eb2f8278 [DELETE]

#### And finally remove a post 

- http://localhost:7000/posts/5ecff97696326339cee9916a [DELETE]